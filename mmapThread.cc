#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <iostream>
#include <thread>
using namespace std; 

void writeThread(int t) 
{ 
	printf("Hi, Write Thread here!\n");
	size_t pagesize = getpagesize();
	unsigned long int memAddress = pagesize * (1 << 20);
	 char * region = (char*)mmap(
	 (void*) (memAddress),   	      // Map from the start of the 2^20th page
	    pagesize,                         // for one page length
	    PROT_READ|PROT_WRITE|PROT_EXEC,
	    MAP_ANON|MAP_PRIVATE,             // to a private block of hardware memory
	    0,
	    0
	  );
	 if (region == MAP_FAILED) {
	    perror("Could not mmap");
	 } else {
		  char writeText[] = "Hey there! This is the Write Thread";
		  for(int i = 0; i < 2; i++) {
			printf("\nWrite thread now writing '%s' to memory location %p\n\n", writeText, (int *)memAddress);
		 	strcpy(region, writeText);
			string s = "How's your day?";
			strcpy(writeText, s.c_str());
			sleep(4);
		  }
		 sleep(30);	
		 printf("Exiting Write Thread\n");
	}
} 

void readThread(int t) {
	printf("This is the Read Thread speaking :)\n");
	size_t pagesize = getpagesize();
	char* ptr;

	sleep(1);
	for(int i = 0; i < 5; i++) {
		ptr = (char *)(pagesize * 1<<20);
		printf("From read thread - contents of %p: %s\n", (void *)ptr, ptr);
		sleep(2);
	}
	unsigned long int memAddress = pagesize * (1 << 20);
        int unmap_result = munmap((void *)(memAddress), 1 << 10);
	if (unmap_result != 0) {
		    perror("Could not munmap");
	}
	printf("Exiting Read Thread\n");

}


int main() 
{ 
	thread th1(writeThread, 3); 
	thread th2(readThread, 3); 

	th1.join(); 
	th2.join(); 

	return 0; 
} 
