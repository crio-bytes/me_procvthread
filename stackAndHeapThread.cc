#include <string.h>
#include <unistd.h>
#include <iostream>
#include <thread>
using namespace std; 

void myHeap(int t) {
	char *i = new char[20];
	strcpy(i, "Hello");
	cout << "Heap of thread " << this_thread::get_id() << " starts at around " << (void*)i << endl;
}

void myStack(int t) {
	int i;
	cout << "Stack of thread " << this_thread::get_id() << " starts at around " << &i << endl;

}

int main(int argc, char* argv[]) 
{ 
	thread th1(myStack, 3); 
	sleep(3);
	thread th2(myStack, 3); 

	// uncomment these out to check heap
	//thread th1(myHeap, 3); 
	//sleep(3);
	//thread th2(myHeap, 3); 

	th1.join(); 
	th2.join(); 

	while(1){};
	return 0; 
} 
