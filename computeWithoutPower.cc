#include <iostream>
#include <chrono> 
#include <stdlib.h>
#include <thread>
#include <cmath>

using namespace std;
using namespace chrono; 

void computeWithoutPower(double start, double end){
	double result = 0;	
	for (double i = start; i >= 0; i--) {		
		result += atan(i) * tan(i);
	};

}

int main(int argc, char**argv) {
	auto start = high_resolution_clock::now(); 

	int base = 10;
	int exp = 8;

	// comment these two lines when running multi-thread
	computeWithoutPower(pow(base, exp), 0);

	// uncomment these 8 lines when running multi-thread
	//thread th1(computeWithoutPower, 0, 2*pow(base, exp-1));
	//thread th2(computeWithoutPower, 2* pow(base, exp-1), 4*pow(base, exp-1));
	//thread th3(computeWithoutPower, 4*pow(base, exp-1), 6*pow(base, exp-1));
	//thread th4(computeWithoutPower, 6*pow(base, exp-1), 8*pow(base, exp-1));
	//thread th5(computeWithoutPower, 8*pow(base, exp-1), 10*pow(base, exp-1));

	//th1.join();
	//th2.join();
	//th3.join();
	//th4.join();
	//th5.join();

	auto stop = high_resolution_clock::now(); 
	auto duration = duration_cast<milliseconds>(stop - start); 
	cout << duration.count() << endl;
}