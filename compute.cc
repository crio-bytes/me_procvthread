#include <iostream>
#include <chrono> 
#include <stdlib.h>
#include <thread>
#include <cmath>

using namespace std;
using namespace chrono; 

void compute(int base, int exp, int offset, int end) {
	double result = 0;	
	for (double i = offset*pow(base, exp); i >= end; i--) {		
		result += atan(i) * tan(i);
	};

}

int main(int argc, char**argv) {
	auto start = high_resolution_clock::now(); 

	int base = 10;
	int exp = 8;

	// comment this line when running multi-thread
	compute(base, exp, 1, 0);

	
	// uncomment these 8 lines when running multi-thread
 	// thread th1(compute, base, exp-1, 2, 0);
	// thread th2(compute, base, exp-1, 4, 2*pow(base, exp-1));
	// thread th3(compute, base, exp-1, 6, 4*pow(base, exp-1));
	// thread th4(compute, base, exp-1, 8, 6*pow(base, exp-1));
	// thread th5(compute, base, exp-1, 10, 8*pow(base, exp-1));
	// th1.join();
	// th2.join();
	// th3.join();
	// th4.join();
	// th5.join();

	auto stop = high_resolution_clock::now(); 
	auto duration = duration_cast<milliseconds>(stop - start); 
	cout << duration.count() << endl;
}