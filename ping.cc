#include <string>
#include <chrono> 
#include <stdlib.h>
#include <thread>
#include <iostream>

using namespace std;
using namespace chrono; 

void readFile() {
    // Fill in a web address to ping
    // Try if it allows ping request by checking on terminal first
	string str = "ping -c 1 <add-some-web-address> 2>&1 > /dev/null ";

	const char *command = str.c_str(); 
	system(command); 
}

void readNtimes(int n) {
	while (n > 0) {
		readFile();
		n--;
	}	
}


int main(int argc, char**argv) {
	auto start = high_resolution_clock::now(); 

	int times = 8;

	// comment these two lines when running multi-thread
	thread th1(readNtimes, times);
	th1.join();


	// uncomment these four lines when running multi-thread
	//thread th1(readNtimes, times/2);
	//thread th2(readNtimes, times/2);

	//th1.join();
	//th2.join();

	auto stop = high_resolution_clock::now(); 
	auto duration = duration_cast<milliseconds>(stop - start); 
	cout << duration.count() << endl;
}