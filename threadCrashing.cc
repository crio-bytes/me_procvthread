#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <thread>
using namespace std; 

void suicideThread(int t) {
	printf("Hey there, I am the suicide thread\n");
	sleep(15);
	printf("SuicideThread: I'm gonna try to access invalid memory now. Sit Tight!\n");
	sleep(1);
	char *p = 0;
	p[0] = '\n';
}

void normalThread(int t) {
	printf("Hi, I am the lazy thread. I'll go to sleep now\n");
	while(1){};
}

int main(int argc, char* argv[]) 
{ 
	thread th1(normalThread, 3); 
	sleep(1);
	thread th2(suicideThread, 3); 

	th1.join(); 
	th2.join(); 

	return 0; 
} 
