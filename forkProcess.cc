#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h> 

int main() 
{ 
	// child process because return value zero 
	if (fork() == 0) {
		printf("Hello from Child!\n"); 
		printf("Child going to deep sleep\n");
		while(1) {};
	}
	// parent process because return value non-zero. 
	else{
		printf("Hello from Parent!\n"); 
		sleep(10);
		printf("Parent going to commit suicide\n");
		char *p = 0;
		p[0] = '\n';
	}
	return 0; 
} 
